package main

import (
	"fmt"

	"github.com/qmuntal/stateless"
)

type TaskTrigger string

const (
	triggerAssigned         TaskTrigger = "Assigned"
	triggerStartWork        TaskTrigger = "Working"
	triggerWorkDone         TaskTrigger = "WorkDone"
	triggerInvoicingStarted TaskTrigger = "Invoicing started"
	triggerInvoicingDone    TaskTrigger = "Invoicing done"
	triggerClosed           TaskTrigger = "Done"
)

type TaskStatus string

const (
	stateNew              TaskStatus = "New"
	stateWaitingForWorker            = "Waiting"
	stateAssigned                    = "Assigned"
	stateWorking                     = "Working"
	stateToBeInvoiced                = "To be invoiced"
	stateInvoiced                    = "Invoiced"
	stateDone                        = "Done"
)

type Task struct {
	TaskObj interface{}
	state   *stateless.StateMachine
}

type TaskDb struct {
	uuid   int
	status string
}

func TaskFromDB(id int) *Task {
	//Get task from db
	var t = TaskDb{uuid: 123, status: string(stateWorking)}

	return &Task{TaskObj: t, state: InitStateMachine(t.status)}
}

func InitStateMachine(state string) *stateless.StateMachine {
	status := stateless.NewStateMachine(state)

	status.Configure(stateNew).
		Permit(triggerClosed, stateDone)

	status.Configure(stateWaitingForWorker).
		Permit(triggerAssigned, stateAssigned)

	status.Configure(stateAssigned).
		Permit(triggerStartWork, stateWorking)

	status.Configure(stateWorking).
		Permit(triggerWorkDone, stateToBeInvoiced)

	status.Configure(stateToBeInvoiced).
		Permit(triggerInvoicingStarted, stateInvoiced)

	status.Configure(stateInvoiced).
		Permit(triggerInvoicingDone, stateDone)

	return status
}

func (t Task) Close() {
	t.state.Fire(triggerClosed)
}

func (t Task) PrintGraph() {
	fmt.Println(t.state.ToGraph())
}

func TestTask() {
	var task = TaskFromDB(123)
	task.Close()
	task.PrintGraph()
}
